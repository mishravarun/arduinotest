package com.varunmishra.arduinotest;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import org.w3c.dom.Text;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    private UsbManager usbManager;
    public TextView txtSpeechInput;
    private UsbSerialDriver device;
    public TextView txtStatus;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    int commandSaid= 0;
    public final static String TAG = "Arduandro";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        ImageButton btnSpeak=(ImageButton)findViewById(R.id.btnSpeak);
        txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);
        txtStatus=(TextView)findViewById(R.id.txtStatus);
        getActionBar().hide();

        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });

    }
    @Override
    protected void onPause() {
        super.onPause();
//check if the device is already closed
        if (device != null) {
            try {
                device.close();
            } catch (IOException e) {
//we couldn't close the device, but there's nothing we can do about it!
            }
//remove the reference to the device
            device = null;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();

//get a USB to Serial device object
        device = UsbSerialProber.acquire(usbManager);
        if (device == null) {
//there is no device connected!
            txtStatus.setTextColor(Color.RED);
            txtStatus.setText("Disconnected");
            Log.d(TAG, "No USB serial device connected.");
        } else {
            try {
//open the device
                device.open();
//set the communication speed
                txtStatus.setTextColor(Color.GREEN);
                txtStatus.setText("Connected");

                device.setBaudRate(115200); //make sure this matches your device's setting!
                if(commandSaid==1)
                    sendToArduino("a");
                if(commandSaid==2)
                    sendToArduino("b");
                commandSaid=0;
            } catch (IOException err) {
                Log.e(TAG, "Error setting up USB device: " + err.getMessage(), err);
                try {
//something failed, so try closing the device
                    device.close();
                } catch (IOException err2) {
//couldn't close, but there's nothing more to do!
                }
                device = null;
                return;
            }
        }
    }
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String speech = result.get(0);
                    if(speech.equalsIgnoreCase("Start")) {
                        txtSpeechInput.setText("Started");
                            commandSaid=1;

                    }
                       // txtSpeechInput.setText("Started");
                    else if(speech.equalsIgnoreCase("stop")) {
                        txtSpeechInput.setText("Stopped");
                        commandSaid=2;
                    }else {
                        txtSpeechInput.setText("Unknown Command");
                        commandSaid=0;
                    }
                }
                break;
            }

        }
    }

    private void sendToArduino(String data){
        byte[] dataToSend = data.getBytes();
//send the color to the serial device
        if (device != null){
            try{
                device.write(dataToSend, 500);
                Toast.makeText(this, "Sent", Toast.LENGTH_SHORT).show();
            }
            catch (IOException e){
                Log.e(TAG, "couldn't write bytes to serial device");
            }
        }
    }

}